# MoviesApp

This demo allow the user to create locally some movies with an image, a description, a name and a release date. 

## External libraries Used
No external libraries other than Angular, RxJS and Material were used.
Material was used just for creating a datepicker component that allows the user to select the release date. 

Sass preprocessor was used in order to compile the classes used on the code. The layout was built with Raw Sass code.


## Code Organization
All the code was written on a module called `movies` so that if further funcionalities are meant to be built, those can be written in different modules.

### Pages folder

Contains the different pages of the project: add-movie, top5 and home.

### Shared folder
There are two main Services under `shared`folder: one is called `MoviesApiService` that serves the purpose of calling the external API for the Top5 section. The other service is called `LocalMovies` that handles the addition and deletion of the movies between different sections. This serves as a local store of the modified information of the movies.

A `mock` folder was created under `shared` that has some test information used on the unit tests on different sections.

A `pipe` was created for giving format to the dates on different sections.

A `types`  folder was defined and contains the definition of types used on the application.

## Running the project

1. Run `npm install`
2. Run `ng serve` for a dev server. 
3. Navigate to `http://localhost:4200/` to view the application running

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Production setup

For the scope of the project, no configuration has been written for production other than the default Angular CLI configuration.