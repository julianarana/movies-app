import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './shared/components/menu/menu.component';
import { HomeComponent } from './pages/home/home.component';
import { MovieItemComponent } from './shared/components/movie-item/movie-item.component';
import { MovieDetailComponent } from './shared/components/movie-detail/movie-detail.component';
import { HttpClientModule } from '@angular/common/http';
import { MoviesRoutingModule } from './movies-routing.module';
import { AddMovieComponent } from './pages/add-movie/add-movie.component';
import { MoviesComponent } from './movies.component';
import { Top5Component } from './pages/top5/top5.component';
import { MatInputModule} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { DatePipe } from './shared/pipes/date.pipe';

@NgModule({
  declarations: [
    MenuComponent,
    HomeComponent,
    MovieItemComponent,
    MovieDetailComponent,
    AddMovieComponent,
    MoviesComponent,
    Top5Component,
    DatePipe,

  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MoviesRoutingModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatInputModule,
  MatFormFieldModule,
  ],
  exports: [
    MoviesComponent,
  ]
})
export class MoviesModule { }
