import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AddMovieComponent } from './pages/add-movie/add-movie.component';
import { Top5Component } from './pages/top5/top5.component';

const moviesRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'add-movie', component: AddMovieComponent },
  { path: 'top-5', component: Top5Component }
];

@NgModule({
  imports: [
    RouterModule.forChild(moviesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MoviesRoutingModule { }
