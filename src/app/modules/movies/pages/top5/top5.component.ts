import { Component, OnInit, OnDestroy } from '@angular/core';
import { Movie } from '../../shared/types/movies';
import { MoviesApiService } from '../../shared/http/movies-api.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-top5',
  templateUrl: './top5.component.html',
  styleUrls: ['./top5.component.scss']
})
export class Top5Component implements OnDestroy, OnInit {

  protected movies: Movie[] = [];
  private moviesSubscription: Subscription;

  constructor(private top5Service: MoviesApiService) { }

  ngOnInit() {
    this.moviesSubscription = this.top5Service.getMovies().subscribe((movies: Movie[]) => {
      this.movies = movies;
    });
  }

  ngOnDestroy() {
    this.moviesSubscription.unsubscribe();
  }

}
