import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Top5Component } from './top5.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Observable } from 'rxjs/internal/Observable';
import { from } from 'rxjs';
import { Movie } from '../../shared/types/movies';
import { movies } from '../../shared/mocks/movies';
import { MoviesApiService } from '../../shared/http/movies-api.service';

class MockTop5Service extends MoviesApiService {
  authenticated = false;

  getMovies(): Observable<Movie[]> {
    const theMovies: Movie[] = movies;
    const moviesPromise: Promise<Movie[]> = Promise.resolve(theMovies);
    return from(moviesPromise);
  }
}

describe('Top5Component', () => {
  let component: Top5Component;
  let fixture: ComponentFixture<Top5Component>;
  let service: MoviesApiService;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    service = new MockTop5Service(null);
    TestBed.configureTestingModule({
      declarations: [Top5Component],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    httpMock = TestBed.get(HttpTestingController);
    service = new MoviesApiService(null);
    component = new Top5Component(service);
    fixture = TestBed.createComponent(Top5Component);
    fixture.detectChanges();
  });

  afterEach(() => {
    service = null;
    component = null;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
