import { Component, OnInit } from '@angular/core';
import { Movie } from '../../shared/types/movies';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { LocalMoviesService } from '../../shared/services/local-movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  protected selected: number;
  protected movies: Movie[] = [];

  protected movie: Movie;

  constructor(private moviesService: LocalMoviesService, private router: Router) { }

  ngOnInit() {
    this.movies = this.moviesService.getMovies();
  }

  protected selectMovie(position: number) {
    this.selected = position;
    if (position >= 0 && position < this.movies.length) {
      this.movie = this.movies[position];
    } else {
      this.movie = null;
    }
  }

  protected addMovie() {
    this.router.navigate(['/add-movie']);
  }

  protected removeMovie() {
    this.moviesService.removeMovie(this.selected);
    this.selected--;
    if (this.selected >= 0) {
      this.movie = this.movies[this.selected];
    } else {
      this.movie = null;
    }
  }

}
