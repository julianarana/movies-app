import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Movie } from '../../shared/types/movies';
import { Router } from '@angular/router';
import { LocalMoviesService } from '../../shared/services/local-movies.service';
@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss']
})
export class AddMovieComponent implements OnInit {

  protected movie: Movie = {
    title: '',
    description: '',
    release: new Date(),
    image: '',
  };

  constructor(private moviesService: LocalMoviesService, private router: Router) { }

  ngOnInit() {
  }

  toBase64(file): Promise<string | ArrayBuffer | null> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }


  async changeImage(event: EventTarget) {
    const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    const files: FileList = target.files;
    const result: string | Error = await this.toBase64(files[0]).catch(e => e);
    if (result instanceof Error) {
      console.log('Error: ', result.message);
      return;
    }
    this.movie.image = result;
  }

  saveMovie() {
    this.moviesService.addMovie(this.movie);
    this.router.navigate(['/home']);
  }
}
