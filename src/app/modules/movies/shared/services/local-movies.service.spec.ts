import { TestBed } from '@angular/core/testing';

import { LocalMoviesService } from './local-movies.service';
import { movies } from '../mocks/movies';
import { Movie } from '../types/movies';

describe('LocalMoviesService', () => {

  let service: LocalMoviesService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(LocalMoviesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add a movie', () => {
    expect(service.movies).toBeTruthy();
    expect(service.movies.length).toEqual(0);
    service.addMovie(movies[0]);
    expect(service.movies.length).toEqual(1);
    service.addMovie(movies[0]);
    expect(service.movies).toBeTruthy();
    expect(service.movies.length).toEqual(2);
  });

  it('should remove a movie', () => {
    service.addMovie(movies[0]);
    service.addMovie(movies[1]);
    service.addMovie(movies[2]);
    expect(service.movies.length).toEqual(3);
    service.removeMovie(1);
    expect(service.movies.length).toEqual(2);
    expect(service.movies[1]).toEqual(movies[2]);
  });

  it('should return the movie set', () => {
    service.addMovie(movies[0]);
    service.addMovie(movies[1]);
    service.addMovie(movies[2]);
    service.addMovie(movies[3]);
    service.addMovie(movies[4]);
    const servicesMovies: Movie[] = service.getMovies();
    servicesMovies.map((movie: Movie, index: number) => {
      expect(movie).toEqual(movies[index]);
    });
  });
});
