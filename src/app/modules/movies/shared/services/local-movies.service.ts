import { Injectable } from '@angular/core';
import { Movie } from '../types/movies';

@Injectable({
  providedIn: 'root'
})
export class LocalMoviesService {

  public movies: Movie[] = [];

  constructor() { }

  public getMovies(): Movie[] {
    return this.movies;
  }

  public addMovie(movie: Movie) {
    this.movies.push(movie);
  }

  public removeMovie(position: number) {
    this.movies.splice(position, 1);
  }
}
