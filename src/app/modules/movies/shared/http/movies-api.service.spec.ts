import { async, TestBed } from '@angular/core/testing';

import { MoviesApiService } from './movies-api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('MoviesApiService', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        HttpClientTestingModule,
      ],
    })
      .compileComponents();
  }));
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MoviesApiService = TestBed.get(MoviesApiService);
    expect(service).toBeTruthy();
  });
});
