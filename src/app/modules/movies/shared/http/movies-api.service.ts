import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Movie } from '../types/movies';

const GET_URL = 'http://www.mocky.io/v2/5dc3c053300000540034757b';

@Injectable({
  providedIn: 'root'
})
export class MoviesApiService {

  constructor(private http: HttpClient) { }

  getMovies(): Observable<Movie[]> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    return this.http.get(GET_URL, { headers })
      .pipe(
        map((result: { movies: any[] }): Movie[] => result.movies.map((movie: any): Movie => {
          const { title, description, release, image } = movie;
          return { title, description, image, release: new Date(release) };
        })),
        catchError((error: string): Observable<never> => throwError(error))
      );
  }
}
