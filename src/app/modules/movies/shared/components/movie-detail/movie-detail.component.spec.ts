import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieDetailComponent } from './movie-detail.component';
import { movie } from '../../mocks/movies';

describe('MovieDetailComponent', () => {
  let component: MovieDetailComponent;
  let fixture: ComponentFixture<MovieDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show an empty descriptor when no movie is given', () => {
    const compiled = fixture.nativeElement;
    const textDescriptor = compiled.querySelector('.empty-text');
    expect(textDescriptor).toBeTruthy();
  });

  it('should not display the details section when no movie is given', () => {
    const compiled = fixture.nativeElement;
    const details = compiled.querySelector('.details');
    expect(details).toBeFalsy();
  });

  describe('Movie information', () => {

    beforeEach(() => {
      component.movie = movie;
      fixture.detectChanges();
    });

    it('should display the details section', () => {
      const compiled = fixture.nativeElement;
      const details = compiled.querySelector('.details');
      expect(details).toBeTruthy();
    });

    it('should display the movie title', () => {
      const compiled = fixture.nativeElement;
      const title = compiled.querySelector('.movie-title');
      expect(title).toBeTruthy();
      expect(title.innerHTML).toBe(movie.title);
    });

    it('should display the movie description', () => {
      const compiled = fixture.nativeElement;
      const description = compiled.querySelector('.movie-description');
      expect(description).toBeTruthy();
      expect(description.innerHTML).toBe(movie.description);
    });
  });
});
