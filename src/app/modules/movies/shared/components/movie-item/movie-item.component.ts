import { Component, OnInit, Input, Output } from '@angular/core';
import { Movie } from '../../types/movies';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.scss']
})
export class MovieItemComponent implements OnInit {

  @Input()
  public movie: Movie;

  @Input()
  public isSelected: boolean;

  constructor() { }

  ngOnInit() {
  }

}
