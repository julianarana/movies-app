import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {

  transform(value: Date): string {
    const day: number = value.getDate();
    const month: number = value.getMonth();
    const year: number = value.getFullYear();
    const dayStr: string = day < 10 ? `0${day}` : day.toString();
    const monthStr: string = month < 10 ? `0${month}` : month.toString();

    return `${dayStr}/${monthStr}/${year}`;
  }

}
