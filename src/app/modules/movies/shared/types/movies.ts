export interface Movie {
  description: string;
  image: string;
  release: Date;
  title: string;
}
